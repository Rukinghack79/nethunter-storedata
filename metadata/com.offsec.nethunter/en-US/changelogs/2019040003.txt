* Bug fixes for KaliServicesFragment and changed the rest of possible getContext() to context and getActivity() to activity after Commit
* Changes NhPaths class to singleton class with implementing SharedPreferences.OnSharedPreferenceChangeListener
* Replaced string "com.offsec.nethunter" to BuildConfig.APPLICATION_ID
